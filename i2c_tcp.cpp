// I2C <-> TCP Tester
// Written By Francesco Martina @2021

#include <iostream>
#include <string>
#include <cstring>
#include <csignal>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

using namespace std;

#define MAX_RX_BUFFER_SIZE (16)
#define DEBUG
//#define WRITE_RESP

///global var

char i2c_dev_file_name[32];
int i2c_dev_file;

sockaddr_in servAddr;
int serverSd;
sockaddr_in clientSockAddr;
int clientSd;

//function prototypes
void init_server_socket();
void client_listener();
void fsm();


/////////////////////////////////////////////////////////////////////////////// AUX

void quit(){
    close(serverSd);
    close(clientSd);
    close(i2c_dev_file);
    cout << "Socket and File Closed. Bye!" << endl;
    exit(1);
}

//closing connection if Ctrl+C
void SIGINT_handler(int s){
    quit();
}


/////////////////////////////////////////////////////////////////////////////// I2C 
/// (TODO success/error in i2c transactions)

void set_i2c_address(unsigned char i2c_device_addr){
    //set the i2c device address
    if (ioctl(i2c_dev_file, I2C_SLAVE, i2c_device_addr) < 0) {
        cout << "Error while setting the I2C device address!";
        quit();
    }
}

void init_i2c(){
    //open the i2c dev file
    i2c_dev_file = open(i2c_dev_file_name, O_RDWR);
    if (i2c_dev_file < 0) {
        cout << "I2C dev file not found!" << endl;
        quit();
    }
    cout << "I2C dev file open successfully" << endl;
}

int i2c_read(unsigned char i2c_device_addr, unsigned char* read_buffer, int len){
    int ret = 0;

#ifdef DEBUG
    cout << "---------------------- RRRRRRRRRRRRRRRRRRRRRRRRRR ---------------------" << endl;
    cout << "Reading " << len << " bytes from I2C device, ";
    cout << "address: 0x" << hex << uppercase << (unsigned int)i2c_device_addr << dec << nouppercase << endl;
#endif

    //perform the i2c reading
    set_i2c_address(i2c_device_addr);
    int i2c_read_bytes = read(i2c_dev_file, read_buffer, len);
    if(i2c_read_bytes != len){
        cout << "Error, byte read: " << i2c_read_bytes << " / " << len << endl;
        ret = 1;
    }

#ifdef DEBUG
    else
        cout << "Reading Success" << endl;
        cout << "Payload : " << hex << uppercase ;
        for(int i=0; i<len; i++)
            cout << (unsigned int)read_buffer[i] << " ";
        cout << dec << nouppercase << endl;
#endif

    return ret;
}

int i2c_write(unsigned char i2c_device_addr, unsigned char* write_buffer, int len){
    int ret = 0;

#ifdef DEBUG
    cout << "---------------------- WWWWWWWWWWWWWWWWWWWWWWWWWW ---------------------" << endl;
    cout << "Writing " << len << " bytes to I2C device, ";
    cout << "address: 0x" << hex << uppercase << (unsigned int)i2c_device_addr << dec << nouppercase << endl;
    cout << "Payload : " << hex << uppercase ;
    for(int i=0; i<len; i++)
        cout << (unsigned int)write_buffer[i] << " ";
    cout << dec << nouppercase << endl;
#endif

    //perform the i2c writing
    set_i2c_address(i2c_device_addr);
    int i2c_wrote_bytes = write(i2c_dev_file, write_buffer, len);
    if(i2c_wrote_bytes != len){
        cout << "Error, byte wrote: " << i2c_wrote_bytes << " / " << len << endl;
        ret = 1;
    }

#ifdef DEBUG
    else
        cout << "Writing Success" << endl;
#endif

    return ret;
}

/////////////////////////////////////////////////////////////////////////////// Socket

//initialise the socket used by server side for listening
void init_server_socket(int listen_port){
    //setup a socket and connection tools
    memset((char*)&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(listen_port);
 
    //open stream oriented socket with internet address
    //also keep track of the socket descriptor
    serverSd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverSd < 0){
        cerr << "Error establishing the server socket" << endl;
        quit();
    }
    
    //bind the socket to its local address
    int bindStatus = ::bind(serverSd, (struct sockaddr*) &servAddr, sizeof(servAddr));
    if(bindStatus < 0){
        cerr << "Error binding socket to local address" << endl;
        quit();
    }
    
    //write diagnostics
    char* server_IP_str;
    server_IP_str = inet_ntoa(servAddr.sin_addr);
    cout << "Server socket successfully binded at IP: " << server_IP_str << ", Port: " << ntohs(servAddr.sin_port) << endl;
}

//client connection manager (an unique client is accepted in this implementation)
void client_listener(){

    //listen forever
    while(1){
        cout << endl << "Writing for client..." << endl;

        //Listen for the client
        listen(serverSd, 1);

        //receive a request from client using accept and create a new socket descriptor
        socklen_t clientSockAddrSize = sizeof(clientSockAddr);
        clientSd = accept(serverSd, (sockaddr *)&clientSockAddr, &clientSockAddrSize);
        if(clientSd < 0){
            cerr << "Error accepting request from client!" << endl;
            quit();
        }
        
        //write diagnostics
        char* client_IP_str;
        client_IP_str = inet_ntoa(clientSockAddr.sin_addr);
        cout << "Client Connected. IP: " <<  client_IP_str << ", Port: " << ntohs(clientSockAddr.sin_port) << endl;
        cout << "Ready for commands" << endl;

        //transactions manager
        fsm();

        //we need to close the socket descriptors after we're all done
        close(clientSd);
        cout << "Client connection closed" << endl;
        cout << "________________________" << endl;
    }
}


///finite state machine for the read/write transactions

//Rx Frame
//Reading from I2C : <(unsigned char) I2C Slave ADD> <(unsigned char) 0x01> <(unsigned int) reading len>
//Writing to I2C : <(unsigned char) I2C Slave ADD> <(unsigned char) 0x00> <(unsigned int) payload len> <(unsigned char) payload_byte1> [<(unsigned char) payload_byte2> ...]

//Tx Frame
//Reading from I2C : <(unsigned char) payload_byte1> [<(unsigned char) payload_byte2> ...]
//Writing to I2C : <(unsigned char) 0x00(success) / 0x01(failed)>

void fsm(){
    unsigned char rx_buffer[MAX_RX_BUFFER_SIZE];
    
    //listen until the client disconnects
    while(1)
    {
        if(!recv(clientSd, rx_buffer, 6, MSG_WAITALL)) break;
        unsigned char i2c_address = rx_buffer[0];
        unsigned int payload_len = *((unsigned int *)&rx_buffer[2]);
        
        if(!rx_buffer[1]){
            ///read

            //allocate the reading buffer and get data from the i2c device
            unsigned char* read_buffer = new unsigned char[payload_len];
            i2c_read(i2c_address, read_buffer, payload_len);

            //send data
            int byte_sent = send(clientSd, read_buffer, payload_len, 0);

#ifdef DEBUG
            cout << "Byte sent to the Client: " << byte_sent << endl;
#endif

            //free the buffer
            delete[] read_buffer;

        }else{
            ///write

            //allocate the payload buffer and get the data from the client
            unsigned char* write_buffer = new unsigned char[payload_len];
            if(!recv(clientSd, write_buffer, payload_len, MSG_WAITALL)){
                break;
                delete[] write_buffer;
            } 

            //write data to the device
            int ret = i2c_write(i2c_address, write_buffer, payload_len);

#ifdef WRITE_RESP
            unsigned char write_resp = ret;
            send(clientSd, &write_resp, sizeof(unsigned char), 0);
#endif

            //free the buffer
            delete[] write_buffer;
        }

    }
}


/////////////////////////////////////////////////////////////////////////////// main

//////////////////////////////////////////////
// first argument is the i2c device file (ex. /dev/i2c-1)
// the second is the listen TCP port

int main(int argc, char *argv[]){

    //rx data buffer
    unsigned char rx_buffer[MAX_RX_BUFFER_SIZE];
    
    cout
    << "----------------------------------------------------------------" << endl
    << "I2C <-> TCP Programmer based on Raspberry PI" << endl
    << "Written by Francesco Martina @2021" << endl
    << "----------------------------------------------------------------" << endl;
    
    //register the SIGINT handler
    signal(SIGINT,SIGINT_handler);

    //get arguments
    if(argc != 3){
        cout << "Arguments must be passed: <I2C Dev File> and <TCP Listen Port>" << endl;
        exit(1);
    }

    strcpy(i2c_dev_file_name, argv[1]);
    int listen_port = atoi(argv[2]);

    //
    init_i2c();
    init_server_socket(listen_port);
    client_listener(); // Forever
    //
    
    return 0;
}
