# TCP <--> I2C simplest bridge with socket server

This is a single-client listening TCP socket to perform I2C accesses.

## Launching command is:
*./i2c_tcp path_to_i2c_dev listening_TCP_port*

Example (tested on Raspberry 3B+ and 4):

*./i2c_tcp /dev/i2c-1 5000*


## The TCP transaction structures are:

**Commands from user to this software (to be transmitted to the socket after connection)**

- Reading from I2C : <(unsigned char) I2C Slave ADD> <(unsigned char) 0x00> <(unsigned int) reading len>

- Writing to I2C : <(unsigned char) I2C Slave ADD> <(unsigned char) 0x01> <(unsigned int) payload len> <(unsigned char) payload_byte1> [<(unsigned char) payload_byte2> ...]

**Commands from the software to the user**

- Reading from I2C : <(unsigned char) payload_byte1> [<(unsigned char) payload_byte2> ...]

- Writing to I2C (*only if WRITE_RESP is defined*) : <(unsigned char) 0x00(success) / 0x01(failed)>



## Add this to /etc/rc.local to launch the software at boot time and create a log file
**start the I2C <-> TCP programmer on port 5000 and  I2C dev 1**
/home/pi/rasp_i2c_tcp/i2c_tcp /dev/i2c-1 5000 > /home/pi/rasp_i2c_tcp/log.txt 2>&1
